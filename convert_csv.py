# -*- coding: utf-8 -*-
""" Create trs-load file from excel/csv  #NAUA-1193
       Takes an csv file and converts it into a trs load file
"""
import os
import sys

import csv
import codecs

from trs.logger import warning


def convert_csv_to_trs(csv_dir, out_trs_file):
    trs_file = codecs.open(out_trs_file, "a", encoding="utf-8")
    # get list of files
    filenames = next(os.walk(csv_dir))[2]
    try:
        for csv_file in filenames:
            # join path of csv files with csv files name
            csv_file = os.path.join(csv_dir, csv_file)
            with open(csv_file, 'r') as csvf:
                csv_reader = csv.reader(csvf, delimiter=",", dialect=csv.excel)
                try:
                    # skip header of read csv file
                    next(csv_reader, None)
                    write_in_trs_file(csv_reader, trs_file, csv_file)
                except csv.Error, e:
                    warning('The file "%s" you try to convert is not a csv file. (%s).'
                            % (csv_file, e))
    finally:
        trs_file.close()


def write_in_trs_file(csv_reader, trs_file, csv_file):
    empty_rows = True
    for row in csv_reader:
        empty_rows = False
        # write only not empty rows in new file
        if len(max(row)) >= 1:
            trs_file.write(create_trs_row(row))
    if empty_rows is True:
        warning('File "%s" was not processed because it was empty.'
                % csv_file)


def create_trs_row(row):
    new_line = u"\n<REC>\n<STM>="
    newrow = []
    for cel in row:
        if cel:
            newrow.append(cel.decode("utf-8"))
    new_line += u";".join(newrow)
    return new_line


def main(args):
    csv_files = args[1]
    trs_output = args[2]
    convert_csv_to_trs(csv_files, trs_output)


if __name__ == "__main__":
    main(sys.argv)
