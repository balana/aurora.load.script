# -*- coding: utf-8 -*-

import StringIO
import unittest

import convert


class TestConversion(unittest.TestCase):

    def test_convert_csv_to_trs(self):
        self.assertEqual(u"\n<REC>\n<STM>=新劳动法", convert.create_trs_row(["新劳动法"]),
                         "Test converting row with single cell failed")

    def test_convert_csv_to_trs_many(self):
        self.assertEqual(convert.create_trs_row(["变更", "更换", "更新", "改变", "变换", "修改"]),
                         u"\n<REC>\n<STM>=变更;更换;更新;改变;变换;修改",
                         "Test converting row with many cells failed")

    def test_convert_csv_to_trs_not_chinese(self):
        self.assertEqual(convert.create_trs_row(["abc", "def", "ghi", "jklmno"]),
                         u"\n<REC>\n<STM>=abc;def;ghi;jklmno",
                         "Test for non Chinese words did not work")

    def test_write_in_trs_file(self):
        csv_file = [["变更", "改变"], ["更换", "更新", "更换"]]
        trs_file = StringIO.StringIO()
        convert.write_in_trs_file(csv_file, trs_file, "csv_file")
        self.assertEqual(trs_file.getvalue(),
                         u"\n<REC>\n<STM>=变更;改变\n<REC>\n<STM>=更换;更新;更换", "Test process csv file failed")

if __name__ == '__main__':
    unittest.main()
